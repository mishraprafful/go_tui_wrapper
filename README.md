# APiWrapper

This is a GO wrapper for making API requests and provide an easy to use TUI

## Installation & setup

* Setting Environment

```bash
export GOPATH=$(pwd)
export PATH="$(pwd)/bin:$PATH"
```

* Installing application

```go
go install app
```

## For issuing a getRequest

* Default URL

```bash
app get
```

* Custom URL

```bash
app get url
```

## For issuing a postRequest

* Default URL

```bash
app post body
```

* Custom URL

```bash
app post url body
```
