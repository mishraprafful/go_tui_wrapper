package getRequest

import (
	"io/ioutil"
	"log"
	"net/http"
)

// MakeRequest makes a request to the specified url
func MakeRequest(url string) {
	resp, err := http.Get(url)
	if err != nil {
		log.Fatalln(err)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	log.Println(string(body))
}
