package main

import (
	"getRequest"
	"postRequest"
	"utils"

	"log"
	"os"
)

func main() {

	// getting cmdline arguments
	argsWithoutProg := os.Args[1:]

	// get request
	if argsWithoutProg[0] == "get" {
		// custom url
		if len(argsWithoutProg) >= 2 {
			// log.Println(argsWithoutProg[1])
			getRequest.MakeRequest(argsWithoutProg[1])
		} else
		// getting url from utils.config
		if len(argsWithoutProg) >= 1 {
			// log.Println(utils.URL)
			getRequest.MakeRequest(utils.URL)
		} else {
			log.Fatal("Something went wrong! Check the request Maybe?")
		}
	} else

	// post request
	if argsWithoutProg[0] == "post" {
		// custom url
		if len(argsWithoutProg) >= 3 {
			postRequest.MakeRequest(argsWithoutProg[1], argsWithoutProg[2])
		} else
		// getting url from utils.config
		if len(argsWithoutProg) >= 2 {
			postRequest.MakeRequest(utils.URL, argsWithoutProg[1])
		} else {
			log.Fatal("Something went wrong! Check the request Maybe?")
		}
	} else {
		log.Fatal("Something went wrong! Check the request Maybe?")
	}

}
