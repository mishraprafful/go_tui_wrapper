package postRequest

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

// MakeRequest makes post requests to the required endpoint
func MakeRequest(url, message string) {

	payload := strings.NewReader(message)
	
	client := &http.Client{}
	req, err := http.NewRequest("POST", url, payload)

	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("Content-Type", "application/json")

	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)

	log.Println(string(body))
}
